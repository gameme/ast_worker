const express = require("express");
const app = express();

const rs_zhuanli = require("./new_code");
// const fst_code_eavel = require("./fts_code_eavel");

var bodyParser = require("body-parser");
app.use(bodyParser());

app.post("/zhuanli", function (req, res) {
  let result = req.body;
  // console.log("result",result)
  let f_script = result.f_script;
  let content = result.content;
  let r = rs_zhuanli.fst_code_eavel(f_script,content);
  res.send(r);
});

// app.post("/fst_code", function (req, res) {
//   let result = req.body;
//   // console.log("result",result)
//   let f_script = result.f_script;

//   let s = fst_code_eavel.get_params(f_script);
//   res.send("200", s);
// });

app.listen(3999, () => {
  console.log("开启服务，端口3999");
});
